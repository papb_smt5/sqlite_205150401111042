package id.ub.sqlite_205150401111042;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "RV";
    EditText etNama;
    EditText etNim;
    Button bInsert;
    Button bView;
    private AppDatabase appDb;

    RecyclerView rv;
    ItemAdapter adapt;
    ArrayList<Item> it;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appDb = AppDatabase.getInstance(getApplicationContext());
        etNama = findViewById(R.id.etNama);
        etNim = findViewById(R.id.etNim);
        bInsert = findViewById(R.id.bInsert);
        bView = findViewById(R.id.bView);
        rv = findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(this));

        bInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Item item = new Item();
                item.setNama(etNama.getText().toString());
                item.setNim(etNim.getText().toString());
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        appDb.itemDao().insertAll(item);
                    }
                });
            }
        });
        bView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        AsyncExample task2 = new AsyncExample(rv);
                        task2.execute();
                    }
                });
            }
        });
    }

    private class AsyncExample extends AsyncTask<Void, Void, List<Item>> {
        RecyclerView rv;

        public AsyncExample(RecyclerView rv) {
            this.rv = rv;
        }

        @Override
        protected List<Item> doInBackground(Void... voids) {
            List<Item> list = appDb.itemDao().getAll();
            return list;
        }

        @Override
        protected void onPostExecute(List<Item> items) {
            super.onPostExecute(items);
            adapt = new ItemAdapter(MainActivity.this, items);
            rv.setAdapter(adapt);
            adapt.notifyDataSetChanged();
        }
    }
}